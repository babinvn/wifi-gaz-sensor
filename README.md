# wifi-gaz-sensor
WiFi gaz sensor using on MQ-2

![WiFi Gaz Sensor](https://user-images.githubusercontent.com/2329753/59553637-1a485c80-8fa0-11e9-9fbe-4da09322f4ab.JPG)

BOM:
- [100x68x50mm Wateproof Enclosure](https://www.aliexpress.com/item/Uxcell-1pcs-64x58x35-83x58x33-100x68x40-100x68x50mm-Wateproof-Electronic-ABS-Junction-Box-Enclosure-with-Clear-Cover-for/32971022907.html)
- [ESP8266 WiFi Kit 8](https://ru.aliexpress.com/item/ESP8266-wifi-0-91-OLED-CP2014-32-Mb-Flash-ESP-8266/32966653064.html)
- [MQ-2 Sensor Module](https://ru.aliexpress.com/item/FREE-SHIPPING-100-New-MQ2-MQ-2-LPG-I-butane-Propane-Methane-Alcohol-Hydrogen-Smoke-Gas/557651417.html)
- [HiLink AC-DC 5V Module](https://ru.aliexpress.com/item/5-pcs-HLK-PM01-AC-DC-220V-to-5V-Step-Down-Power-Supply-Module-Intelligent-Household/32642255110.html)

ESP8266 A0 voltage is 3.3V while MQ-2 uses 5V, so use voltage divider (I use 4K7 and 3K3 resistors).

Usage:

```
$ curl http://gaz-sensor.local/sensor
21
```
Or using Zabbix:
```
$ zabbix_get -s 192.168.1.105 -k iot.sensors.gaz.raw
21
```
OLED library is borrowed from [this post](https://www.mikrocontroller.net/topic/438027)
Heltec ESP8266 board instructions are from [this project](https://github.com/Heltec-Aaron-Lee/WiFi_Kit_series)
