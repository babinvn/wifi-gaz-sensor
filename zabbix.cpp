#include <Arduino.h>
#include "zabbix.h"

#define ZABBIX_AGENT_DEFAULT_PORT 10050
#define STACK_PROTECTOR  512 // bytes

const String NO_COMMAND = "";

ZabbixAgent::ZabbixAgent(){
  server = new WiFiServer(ZABBIX_AGENT_DEFAULT_PORT);
}

ZabbixAgent::ZabbixAgent(int port){
  server = new WiFiServer(port);
}

ZabbixAgent::~ZabbixAgent(){
  delete server;
}

void ZabbixAgent::begin(){
  server->begin();
  server->setNoDelay(true);
}

void ZabbixAgent::handleGet(){
  if (server->hasClient()) {
    WiFiClient client = server->available();
    if (client){
      String command = getCommand(client);
      if (command != NO_COMMAND){
        //Serial.print("> ");
        //Serial.println(command);
        String response = processCommand(command);
        //Serial.print("< ");
        //Serial.println(response);
        respond(client, response);
      }
    }
  }
}

String ZabbixAgent::getCommand(WiFiClient client){
  if (!client.available() || client.read() != 'Z'){
    readAll(client);
    return NO_COMMAND;
  }
  if (!client.available() || client.read() != 'B'){
    readAll(client);
    return NO_COMMAND;
  }
  if (!client.available() || client.read() != 'X'){
    readAll(client);
    return NO_COMMAND;
  }
  if (!client.available() || client.read() != 'D'){
    readAll(client);
    return NO_COMMAND;
  }

  if (!client.available() || client.read() != 1){ /* Protocol version should be 0x1 */
    readAll(client);
    return NO_COMMAND;
  }

  ZBXD_SIZE zbxSize;
  for (int i = 0; i < sizeof(zbxSize.as_array); i++){
    if (!client.available())
      return NO_COMMAND;
    else
      zbxSize.as_array[i] = client.read();
  }

  size_t messageSize = (size_t)zbxSize.as_very_long;
  String command;
  for (int i = 0; i < messageSize; i++){
    if (client.available()){
      command.concat((char)client.read());
    } else {
      break;
    }
  }

  readAll(client);
  return command;
}

void ZabbixAgent::readAll(WiFiClient client){
  while (client.available()){
    client.read();
  }
}

void ZabbixAgent::respond(WiFiClient client, String response){
  if (client.availableForWrite() < 13 + response.length()){
    return;
  }
  
  client.write('Z');
  client.write('B');
  client.write('X');
  client.write('D');

  client.write(1); /* Protocol */

  ZBXD_SIZE zbxSize;
  zbxSize.as_very_long = response.length();
  client.write(zbxSize.as_array, sizeof(zbxSize.as_array));

  for (int i = 0; i < response.length(); i++){
    client.write(response.charAt(i));
  }  
}
