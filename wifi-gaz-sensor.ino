#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "oled.h"
#include "zabbix.h"

#ifndef STASSID
#define STASSID "SSID"
#define STAPSK  "secret"
#endif

#define HOST "gaz-sensor"
#define NAME "WiFi Gaz Sensor"
#define VERSION "0.0.2"
#define CONNECT "Connecting to WiFi..."
#define WELCOME "WiFi Gaz Sensor ver 0.0.2\nSource code available at https://github.com/babinvn/wifi-gaz-sensor"
#define NOT_FOUND "File not found"
#define WARMING_UP "Warming up, please wait"
#define WARMUP_COUNT 4000  // 3 minutes to warm up MQ-2 (about 4000 loops more or less)
#define WARMUP_SECONDS 180l
#define MDNS_STARTED "MDNS responder started"
#define ZABBIX_STARTED "Zabbix agnet started"
#define HTTP_STARTED "HTTP server started"
#define STATUS_NORMAL "OK"
#define STATUS_WARNING "Too High"
#define STATUS_WARMING_UP "Warming up"

const char* ssid = STASSID;
const char* password = STAPSK;
const char* host = HOST;
const char* msgName = NAME;
const char* msgConnecting = CONNECT;
const char* welcome = WELCOME;
const char* errNotFound = NOT_FOUND;
const char* errWarmingUp = WARMING_UP;
const char* msgMDNSStarted = MDNS_STARTED;
const char* msgZabbixStarted = ZABBIX_STARTED;
const char* msgHttpStarted = HTTP_STARTED;
const char* statusNormal = STATUS_NORMAL;
const char* statusWarning = STATUS_WARNING;
const char* statusWarmingUp = STATUS_WARMING_UP;
const int led = 13;

ZabbixAgent zabbixAgent;
ESP8266WebServer server(80);
OLED display = OLED(4, 5, 16);

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", welcome);
  digitalWrite(led, 0);
}

void handleNotFound() {
  digitalWrite(led, 1);
  server.send(404, "text/plain", errNotFound);
  digitalWrite(led, 0);
}

int buf[8];
int ptr = 0;
uint16_t counter = 0;
char err_503_buf[10];
char ok_200_buf[10];

int getSensorValue(){
  int val = 0, i = 0;
  for (; i < sizeof(buf) / sizeof(int); i++) {
    val += buf[i];
  }
  return val / i;
}

void handleSensor() {
  digitalWrite(led, 1);
  if (counter < WARMUP_COUNT) {
    sprintf(err_503_buf, "%d", WARMUP_SECONDS * (WARMUP_COUNT - counter) / WARMUP_COUNT);
    server.sendHeader("Retry-After", err_503_buf);
    server.send(503, "text/plain", errWarmingUp);
  } else {
    sprintf(ok_200_buf, "%d", getSensorValue());
    server.send(200, "text/plain", ok_200_buf);
  }
  digitalWrite(led, 0);
}

char screen[3][24];

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);

  Serial.begin(115200);
  Serial.println("");
  Serial.println(welcome);
  Serial.println("");
  Serial.println(msgConnecting);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  delay(1000);

  display.init();
  display.draw_string(4, 12, msgName);
  display.draw_string(4, 22, msgConnecting);
  display.display();

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  String ip = String("IP:") + WiFi.localIP().toString();
  ip.toCharArray(screen[0], 24);
  Serial.println(screen[0]);

  if (MDNS.begin(host)) {
    Serial.println(msgMDNSStarted);
  }

  zabbixAgent.begin();
  Serial.println(msgZabbixStarted);

  server.on("/", handleRoot);
  server.on("/sensor", handleSensor);
  server.onNotFound(handleNotFound);
  server.begin();

  Serial.println(msgHttpStarted);
  display.scroll_up(32, 20);
}

void loop(void) {
  server.handleClient();
  zabbixAgent.handleGet();
  MDNS.update();

  if (counter < WARMUP_COUNT) {
    counter++;
  }
  buf[ptr++] = analogRead(A0);
  if (ptr >= sizeof(buf) / sizeof(int)) {
    ptr = 0;
  }

  String ip = String("IP:") + WiFi.localIP().toString();
  ip.toCharArray(screen[0], 24);

  int val = 0, i = 0;
  for (; i < sizeof(buf) / sizeof(int); i++) {
    val += buf[i];
  }
  val /= i;

  if (counter < WARMUP_COUNT) {
    sprintf(screen[1], "Status: %s", statusWarmingUp);
  } else if (val > 150) {
    sprintf(screen[1], "Status: %s", statusWarning);
  } else {
    sprintf(screen[1], "Status: %s", statusNormal);
  }

  sprintf(screen[2], "Raw sensor: %5d", val);

  display.clear();
  display.draw_string(4, 4, screen[0]);
  display.draw_string(4, 14, screen[1]);
  display.draw_string(4, 24, screen[2]);
  display.display();
}

String ZabbixAgent::processCommand(String command){
  if (command == "agent.ping"){
    return "1";
  } else if (command == "agent.hostname"){
    return HOST;
  } else if (command == "agent.version"){
    return VERSION;
  } else if (command == "iot.sensors.gaz.raw"){
    return String("") + getSensorValue();
  } else {
    return "ZBX_NOTSUPPORTED: Unsupported item key.";
  }
}
