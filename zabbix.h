#ifndef ZABBIX_H
#define ZABBIX_H

#include <ESP8266WiFi.h>

#define MAX_SRV_CLIENTS 8

union ZBXD_SIZE {
  byte as_array[8];
  uint64_t as_very_long;
};

class ZabbixAgent {
  private:
    int port;
    WiFiServer* server;
    String getCommand(WiFiClient client);
    void readAll(WiFiClient client);
    void respond(WiFiClient client, String response);

  public:
    ZabbixAgent();
    ZabbixAgent(int port);
    virtual ~ZabbixAgent();
    void begin();
    void handleGet();
    virtual String processCommand(String command);
};


#endif /* ZABBIX_H */
